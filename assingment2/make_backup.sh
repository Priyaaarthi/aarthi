#!/bin/bash
#
## creates backup
#

##variables
LOG_LOC="/var/log/mybackup.log"
##

function check_dir_loc {
	#check
	if [ ! -s "/backup_dirs.conf" ]
	then
                echo "please create one"
		exit 1
	fi
}

function check_backup_loc {
        if [ ! -s "/backup_loc.conf" ]
        then
                echo "please create one path"
                exit 1
        fi

}
function check_schedule {
        if [ ! -s "/etc/cron.weekly/make_backup" ]
        then

               #copy to cron.weekly dir
	       sudo cp make_backup.sh /etc/cron.weekly/make_backup
	       echo "the backup schedule"
	       exit 1
        fi
}
function perform_backup {
       #get backup location
       backup_path=$(cat /backup_loc.conf)
       echo "statring backup...."> $LOG_LOC
       while read dir_path
       do
	       #get backup dir name
	       dir_name=$(basename $dir_path)

	       #create filename for compressed backup
	       filename=$backup_path$dir_name.tar.gz

	       #archive dirs and compress
	       tar -zcf $filename $dir_path 2>> $LOG_LOC

	       #change ownership of backup files
	       chown captain:acptain $filename

	       echo "backing up of $dir_name completed." >> $LOG_LOC
       done < /backup_dirs.conf

       echo "backup complete at :">> $LOG_LOC

       date >> $LOG_LOC
}
check_dir_loc
check_backup_loc
check_schedule
perform_backup
