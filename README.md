# Project Name - Aarthi S(@Priyaaarthi)

# Repository Name - Aarthi




## Ass1 Folder(Assignment 1)

### Description  

Install apache web server on Ubuntu and configure it to host two fake website on two ports.
http://localhost:8081
http://localhost:8082

## Process

First -- Install apache2 using following commnad **sudo apt-get install apache2**.

Second -- Goes into **/var/www** directory.

Third -- Create one directory(Here I create on the name of **aarthi** ).

Fourth -- Go into newly created directory and there create a html file for displaying content of the webpage(Here I create on the name of **sample for port 8081** and **sample1 for port 8082**).

Fifth -- Go into **/etc/apache2/site-available** and edit **000.default.conf** for setting which port display which html page.

Sixth -- Edit the ports.conf file to listen the port number(8081,8082)

Seventh -- Then starts the apache server using the command **service apache2**

Finaly -- Open web browser and type **localhost:8081** and **localhost:8082** 




## Assignment 2

### Description


Write a bash shell script to backup any specified directory on a daily,weekly and monthly basis to remote server.

## Process

First -- Create a script(Here i named as **make_backup.sh**).This script is used to automaticaly backup the selected directories on a weekly basis.
 
Second -- Write the code inside the script for backup.

Third -- run the file and see the backup result.



## Assignment 3

### Description

Write a Make file to build a sample project with five C source files. Demonstrate building the project using GCC ang LLVM compilers.

## Process

First -- Create five c files.Here i create c files in the name of **1.pg1.c**,**function1.c**
**function2.c**,**function3.c**,**function4.c**,**function5.c**.And the output files in the name of **output**,**output1**.

Second -- Create Makefile using **gedit Makefile**.

Third -- Run the Makefile using **make**.

Finaly see the output uisng **./output** and **./output1**.




## Assignment 4

### Description

Design a GUI form for student registration using any technology like Qt,Java,Python.

## Process

Here i use Qt designer for creating a registration form.

In qt designer drag and drop the needed fields like label,textbox,checkbox,radio button,drop down menu scroll bar and etc..

Add the backend to connect database and other backend process.










